class HomeController < ApplicationController
  def index
    file = File.read("public/temp.json") if File.exist?("public/temp.json")
    data = file.blank? ? [] : JSON.parse(file)
    @scrape_data = data
    @scrape_data = Kaminari.paginate_array(data).page(params[:page]).per(10) if data.any?
  end
end
