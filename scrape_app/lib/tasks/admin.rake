namespace :admin do
  desc "Scrape Data from National Universities Rankings"
  task scrape_data: :environment do
    puts "Scrapping data from National Universities Rankings started!"
    scraper = Mechanize.new
    info = []
    next_page = "Next"
    name = []
    rank = []
    tuition_fees = []
    total_enrollment = []
    acceptance_rate = []
    average_retention_rates = []
    graduation_rate = []

    page_no = 1
    while(next_page.include? "Next")

      # URL for scraping
      url = "http://colleges.usnews.rankingsandreviews.com/best-colleges/rankings/national-universities/data/page+#{page_no}"
      # Scrape Page.
      scraper.get(url) do |page|
        # Get data from CSS classname attribute from Table structure.
        name = page.search('tbody').search('.school-name').map{ |n| n.text.strip }
        rank = page.search('tbody').search('.rankscore-bronze').map{ |n| n.text.strip }
        tuition_fees = page.search('tbody').search('.search_tuition_display').map{ |n| n.text.strip }
        total_enrollment = page.search('tbody').search('.total_all_students').map{ |n| n.text.strip }
        acceptance_rate = page.search('tbody').search('.r_c_accept_rate').map{ |n| n.text.strip }
        average_retention_rates = page.search('tbody').search('.r_c_avg_pct_retention').map{ |n| n.text.strip }
        graduation_rate = page.search('tbody').search('.r_c_avg_pct_grad_6yr').map{ |n| n.text.strip }

        # Create Hash
        (0..name.size-1).each do |i|
          data = {}
          data[:name] = name[i]
          data[:rank] = rank[i]
          data[:tuition_fees] = tuition_fees[i]
          data[:total_enrollment] = total_enrollment[i]
          data[:acceptance_rate] = acceptance_rate[i]
          data[:average_retention_rates] = average_retention_rates[i]
          data[:graduation_rate] = graduation_rate[i]
          info.append data
        end

        # checking next page
        next_page = page.search('p').search('#pagination').text
      end
      #counter fo page number
      puts "From: page# #{page_no}"
      page_no = page_no + 1
    end

    # Overwrite JSON data to file.
    File.open("public/temp.json","w+") {|f| f.write(JSON.pretty_generate(info)) }

    puts "Scrapping Completed!"
  end
end
