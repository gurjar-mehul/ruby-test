class Calender
  def self.get_free_slot(google_cal, ical)
    calender = google_cal + ical
    free_slot = []
    sort_cal = calender.sort_by { |k| k[:start] }.uniq
    unless sort_cal.empty?
      (0..sort_cal.size-1).each do |time|
        timeslot = sort_cal[0]
        free_slot << timeslot
        sort_cal.delete_if{ |st| st[:end] <= timeslot[:end] }
      end
    end
    free_slot.compact
  end
end

google_cal = [{start: "2015-11-01T10:00:00.00+08:00",end: "2015-11-01T11:00:00.00+08:00"},{start: "2015-11-01T11:00:00.00+08:00",end: "2015-11-01T14:00:00.00+08:00"},{start: "2015-11-01T15:00:00.00+08:00",end: "2015-11-01T17:00:00.00+08:00"}]
ical = [{start: "2015-11-01T12:00:00.00+08:00",end: "2015-11-01T13:00:00.00+08:00"},{start: "2015-11-01T13:00:00.00+08:00",end: "2015-11-01T14:00:00.00+08:00"},{start: "2015-11-01T14:00:00.00+08:00",end: "2015-11-01T15:00:00.00+08:00"},{start: "2015-11-01T15:00:00.00+08:00",end: "2015-11-01T16:00:00.00+08:00"}]

puts Calender.get_free_slot(google_cal,ical)
