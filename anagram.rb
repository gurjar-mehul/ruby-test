module AreAnagrams
  def self.are_anagrams?(string_a, string_b)
    counts = ->(word) { word.chars.reduce(Hash.new 0) {|h, c| h[c] +=1 unless c =~ /\s/; h} }
    counts[a] == counts[b]
  end
end