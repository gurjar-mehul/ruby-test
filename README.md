== README

This Solution for all 3 problems.

Problem#1 Anagrams.

*  AreAnagrams.are_anagrams?('momdad', 'dadmom')

Problem#2 Time Merge

*  Calender.get_free_slot(google_cal,ical)

Problem#3 Scrape Data.

Run below commands.:

* clone this repository.

* go to scrape_data directory, 'cd scrape_data'

* bundle install

* rake admin:scrape_data